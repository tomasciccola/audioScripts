#!/usr/bin/python

import json
import os
import sys
import time

import freesound

API_KEY = os.environ['SOUNDCLOUD_API_KEY']
CLIENT_ID = os.environ["SOUNDCLOUD_CLIENT_ID"] 

N_LINKS_PER_PAGE = 15

last_page = 0

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

client = freesound.FreesoundClient()
client.set_token(API_KEY)

TAG = 'drum'

results = client.text_search(filter="duration:[0.1 TO 1.0]",sort="rating_desc",fields="id,name,previews,tags")

print(bcolors.HEADER + "Numero de resultados:" + bcolors.ENDC)
print(bcolors.OKGREEN + str(results.count) + bcolors.ENDC)

n_pages = results.count / N_LINKS_PER_PAGE
n_pages = 134 #numero arbitrario de paginas para 2000 audios
pages_range = n_pages - last_page
at_page = 0

#movete hasta donde quiero primero (en el caso de haber cortado a la mitad)
'''
while at_page <= last_page:
    results = results.next_page()
    at_page+=1
    print(bcolors.OKBLUE+ str(at_page) + bcolors.ENDC)
    print(bcolors.OKGREEN + str(last_page) + bcolors.ENDC)
    time.sleep(2)

'''

if not os.path.exists("audios"):
    os.makedirs("audios")

if not os.path.exists("jsons"):
    os.makedirs("jsons")

for i in range(pages_range):

    print(bcolors.OKBLUE) 
    print("Pagina: " + str(at_page) + " de: " + str(n_pages))
    print(bcolors.ENDC)

    for sound in results:
        name,ext = os.path.splitext(sound.name) 
        name = name.replace("/","")
        print(bcolors.HEADER + "guardando " + name + '.mp3' + bcolors.ENDC)


        sound.retrieve_preview("audios/", name + '.mp3')
        soundObj = {"fileName":name,"tags":sound.tags}

        with open('jsons/' + name + '.json','w') as outfile:
            json.dump(soundObj,outfile)

    at_page+=1 

    results = results.next_page()
